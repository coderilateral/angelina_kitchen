package com.example.angelinaskitchen.Fragments


import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import coil.compose.AsyncImage
import com.example.angelinaskitchen.Models.Items
import com.example.angelinaskitchen.Models.SharedViewModel
import com.example.angelinaskitchen.R

@Composable
fun ItemsCard(items: Items, viewModel: SharedViewModel, navController: NavController){
    ElevatedCard(
        elevation = CardDefaults.cardElevation(
            defaultElevation = 2.dp
        ),colors = CardDefaults.cardColors(containerColor = Color.White),
        modifier = Modifier
            .size(width = 180.dp, height = 230.dp)
    , onClick = {
        viewModel.id = items.id
        navController.navigate("screen_E")

    }){
        Column(modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .padding(8.dp)
            .background(Color.White), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center) {
            ItemsImages(items = items)
           Column(modifier = Modifier
               .fillMaxWidth()
               .padding(5.dp), horizontalAlignment = Alignment.Start, verticalArrangement = Arrangement.spacedBy(5.dp)) {
               Text(fontWeight = FontWeight.SemiBold,fontSize = 22.sp,text = items.name)
               Text(fontSize = 16.sp,text = "₱ ${items.price}")
           }
        }
    }
}
@Composable
private fun ItemsImages(items: Items){

        AsyncImage(model = items.image, contentDescription = "", modifier = Modifier
            .padding(5.dp)
            .size(120.dp), contentScale = ContentScale.Crop )
}



@Preview (showBackground = true)
@Composable
private fun LayoutPreview() {
    val navController = rememberNavController()
    ItemsCard(items = Items(
        id = "",
        name ="Freelan",
        price = 0,
        image = R.drawable.meat.toString()
    ),viewModel() ,navController)
}



