package com.example.angelinaskitchen.Fragments

import android.util.Log
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.angelinaskitchen.Models.CartData
import com.example.angelinaskitchen.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

@Composable
fun Cart(navController: NavController) {
    val auth = FirebaseAuth.getInstance()
    val userId = auth.currentUser?.uid ?: ""
    var dataset by remember { mutableStateOf<List<CartData>>(emptyList()) }
    val database = FirebaseDatabase.getInstance()
    val cartRef = database.getReference("Cart").child(userId)

    // Fetch cart data from Firebase
    LaunchedEffect(userId) {
        cartRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val newDataset = mutableListOf<CartData>()
                    val productIds = snapshot.children.map { it.key.toString() }

                    // Fetch product details for each cart item
                    productIds.forEach { productId ->
                        database.getReference("menu").child(productId).get().addOnSuccessListener { productSnapshot ->
                            val name = productSnapshot.child("name").getValue(String::class.java) ?: ""
                            val quantity = snapshot.child(productId).child("quantity").getValue(Int::class.java) ?: 0
                            val price = productSnapshot.child("price").getValue(Int::class.java) ?: 0
                            val image = Utils().urlBuilder(productSnapshot.key.toString())
                            val type = productSnapshot.child("type").getValue(String::class.java) ?: ""

                            newDataset.add(
                                CartData(
                                id = productId,
                                name = name,
                                quantity = quantity,
                                price = price,
                                image = image,
                                type = type
                            )
                            )

                            // Update the dataset when all items are processed
                            if (newDataset.size == productIds.size) {
                                dataset = newDataset
                            }
                        }.addOnFailureListener {
                            Log.e("FirebaseError", "Failed to fetch product details")
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("DatabaseError", error.message)
            }
        })
    }

    // Filter out items with quantity less than or equal to 0
    val filteredDataset = dataset.filter { it.quantity > 0 }
    val subtotal = filteredDataset.sumOf { it.quantity * it.price }

    Scaffold(bottomBar = { BottomNavbar(navController, "Cart") }) { innerPadding ->
        Column(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(modifier = Modifier.padding(15.dp))
            Text(
                text = "My Cart",
                fontWeight = FontWeight.SemiBold,
                fontSize = 22.sp,
                textAlign = TextAlign.Center
            )
            Spacer(modifier = Modifier.padding(10.dp))
            LazyColumn(
                modifier = Modifier
                    .height(510.dp)
                    .fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally
            ) {
                items(items = filteredDataset) { item ->
                    Spacer(modifier = Modifier.padding(5.dp))
                    CartCard(item) { cartItem ->
                        val updatedQuantity = cartItem.quantity - 1
                        dataset = dataset.map {
                            if (it.id == cartItem.id) it.copy(quantity = updatedQuantity)
                            else it
                        }.filter { it.quantity > 0 }
                    }
                }
                item { Spacer(modifier = Modifier.height(5.dp)) }
            }
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight()
                    .padding(10.dp), horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(text = "Details", fontWeight = FontWeight.SemiBold, fontSize = 16.sp)
                Column(modifier = Modifier.padding(16.dp)) {
                    Row(horizontalArrangement = Arrangement.SpaceBetween) {
                        Text(text = "Total Items:")
                        Text(text = filteredDataset.sumOf { it.quantity }.toString())
                    }
                    Row(horizontalArrangement = Arrangement.SpaceBetween) {
                        Text(text = "Subtotal:")
                        Text(text = "$$subtotal")
                    }
                    Row(horizontalArrangement = Arrangement.SpaceBetween) {
                        Text(text = "Total:")
                        Text(text = "$$subtotal") // Adjust as needed for total calculations
                    }
                }
                Button(
                    modifier = Modifier.size(width = 200.dp, height = 45.dp),
                    shape = RoundedCornerShape(5.dp),
                    colors = ButtonDefaults.buttonColors(
                        containerColor = Color(0xff000000)
                    ),
                    onClick = {
                        navController.navigate("screen_A")
                    }
                ) {
                    Text(text = "Checkout")
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun LayoutPreview() {
    val navController = rememberNavController()
    Cart(navController)
}
