package com.example.angelinaskitchen.Fragments


import AuthViewModel
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.angelinaskitchen.Models.SharedViewModel
import com.example.angelinaskitchen.Models.UidViewModel

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        installSplashScreen()
        setContent {
            val authViewModel: AuthViewModel by viewModels()
            val uidViewModel = ViewModelProvider(this)[UidViewModel::class.java]
            val viewModel = ViewModelProvider(this)[SharedViewModel::class.java]
            val navController = rememberNavController()
            NavHost(navController = navController, startDestination = "screen_A") {
                composable("screen_A") {
                    LogIn(navController, authViewModel)
                }
                composable("screen_B") {
                    Home(navController, viewModel, authViewModel)
                }
                composable("screen_C") {
                    SignUp(navController, authViewModel)
                }
                composable("screen_D") {
                    Meals(navController, viewModel)
                }
                composable("screen_E") {
                    ItemPreview(navController, viewModel)
                }
                composable("screen_F") {
                    Profile(navController, authViewModel)
                }
                composable("screen_G") {
                    Cart(navController)
                }
                composable("screen_H") {
                    Message(navController,uidViewModel, authViewModel)
                }
                composable("screen_I"){
                    Reservation()
                }
                composable("Admin"){
                    Admin(navController,uidViewModel,authViewModel )
                }
            }
        }
    }
}

