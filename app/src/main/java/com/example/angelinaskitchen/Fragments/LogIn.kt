package com.example.angelinaskitchen.Fragments

import AuthState
import AuthViewModel
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.angelinaskitchen.R

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LogIn(navController: NavController, authViewModel: AuthViewModel) {
    val authState by authViewModel.authState.observeAsState()
    val context = LocalContext.current

    Scaffold(
    ) { innerPadding ->
        Column(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize()
                .background(color = Color(0xFFFFFFFF)), // Fill the maximum available size
            horizontalAlignment = Alignment.CenterHorizontally // Horizontally center content
        ) {
            Image(
                painter = painterResource(id = R.drawable.logo),
                contentDescription = "logo",
                modifier = Modifier.size(360.dp)// Set the size of the image
            )
            Spacer(modifier = Modifier.height(20.dp))
            var email by remember { mutableStateOf("") }
            OutlinedTextField(
                modifier = Modifier.width(350.dp),
                value = email,
                onValueChange = { email = it },
                label = { Text(text = "Email") },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    unfocusedLabelColor = Color.Black,
                    focusedLabelColor = Color.Black,
                    unfocusedBorderColor = Color.Black,  // Setting the unfocused border color to black
                    focusedBorderColor = Color.Black     // Setting the focused border color to black
                )
            )
            Spacer(modifier = Modifier.height(18.dp))

            var password by remember { mutableStateOf("") }
            var showPassword by remember { mutableStateOf(false) }
            val passwordVisualTransformation = remember { PasswordVisualTransformation() }

            OutlinedTextField(
                modifier = Modifier.width(350.dp),
                value = password,
                enabled = true,
                onValueChange = { password = it },
                label = { Text(text = "Password") },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    unfocusedLabelColor = Color.Black,
                    focusedLabelColor = Color.Black,
                    unfocusedBorderColor = Color.Black,  // Setting the unfocused border color to black
                    focusedBorderColor = Color.Black     // Setting the focused border color to black
                ),
                placeholder = { Text(text = "Password") },
                visualTransformation = if (showPassword) {
                    VisualTransformation.None
                }else {
                    passwordVisualTransformation
                }, keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password)
                ,trailingIcon = { Icon( if (showPassword) {
                    Icons.Filled.Visibility
                } else {
                    Icons.Filled.VisibilityOff
                },
                    contentDescription = "Toggle password visibility",
                    modifier = Modifier.clickable { showPassword = !showPassword })
                }
            )

            Spacer(modifier = Modifier.height(45.dp))

            Button(modifier = Modifier.size(width = 150.dp, height = 45.dp),
                shape = RoundedCornerShape(5.dp),
                colors = ButtonDefaults.buttonColors(
                    containerColor = Color(0xff000000)
                ),
               onClick = {
                    authViewModel.Login(email, password)
                },
                enabled = authState != AuthState.Loading
            ) {
                Text("Log in")
            }

            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                Text(fontSize = 12.sp, text = "Don't have an account?")
                TextButton(onClick = { navController.navigate("screen_C") }) {
                    Text(text = "Sign Up", fontSize = 12.sp, color = Color(0xff0317fc))
                }
            }
        }

        LaunchedEffect(authState) {
            when (authState) {
                is AuthState.Authenticated -> {
                    Log.d("LogInScreen", "Navigating to screen_B")
                    navController.navigate("screen_B") {
                        popUpTo("screen_A") { inclusive = true }
                    }
                }
                is AuthState.AdminAuthenticated -> {
                    Log.d("LogInScreen", "Navigating to Admin Screen")
                    navController.navigate("Admin") {
                        popUpTo("screen_A") { inclusive = true }
                    }
                }

                is AuthState.Error -> {
                    val errorMessage = (authState as AuthState.Error).message
                    Log.d("LogInScreen", "Authentication error: $errorMessage")
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
                }

                else -> Log.d("LogInScreen", "authState: $authState")
            }
        }
//
//        LaunchedEffect(authState) {
//            when (authState) {
//                is AuthState.AdminAuthenticated -> {
//                    Log.d("LogInScreen", "Navigating to Admin Screen")
//                    navController.navigate("Admin") {
//                        popUpTo("screen_A") { inclusive = true }
//                    }
//                }
//
//                is AuthState.Authenticated -> {
//                    Log.d("LogInScreen", "Navigating to Home Screen")
//                    navController.navigate("screen_B") {
//                        popUpTo("screen_A") { inclusive = true }
//                    }
//                }
//
//                is AuthState.Error -> {
//                    val errorMessage = (authState as AuthState.Error).message
//                    Log.d("LogInScreen", "Authentication error: $errorMessage")
//                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
//                }
//                else -> {
//                    Log.d("LogInScreen", "Unexpected authState: $authState")
//                }
//            }
//        }

    }
}

@Preview(showBackground = true)
@Composable
private fun LayoutPreview() {
    val navController = rememberNavController()
    LogIn(navController, AuthViewModel())

}
