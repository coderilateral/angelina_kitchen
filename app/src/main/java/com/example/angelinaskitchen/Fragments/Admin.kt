package com.example.angelinaskitchen.Fragments

import AuthViewModel
import android.util.Log
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.angelinaskitchen.Models.Items
import com.example.angelinaskitchen.Models.UidViewModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


@Composable
fun Admin (navController: NavController,uidViewModel: UidViewModel,authViewModel: AuthViewModel){
    val database = FirebaseDatabase.getInstance()
    val messageDb = database.getReference("Messages")
    var dataset by remember { mutableStateOf(listOf<UserMessageData>()) }
    val authState by authViewModel.authState.observeAsState()

    LaunchedEffect(authState) {
        when (authState) {
            is AuthState.Unauthenticated -> {
                Log.d("AdminScreen", "Navigating to screen_A")
                navController.navigate("screen_A") {
                    popUpTo(navController.graph.startDestinationId) { inclusive = true }
                }
            }

            is AuthState.Authenticated -> {
                Log.d("ProfileScreen", "User is authenticated as client")
                // Here you can handle what happens when the client is authenticated
                // For instance, you can load the profile data or update UI accordingly
            }

            is AuthState.Error -> {
                val errorMessage = (authState as AuthState.Error).message
                Log.d("ProfileScreen", "Authentication error: $errorMessage")
//                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
            }

            else -> {
                Log.d("ProfileScreen", "Unexpected authState: $authState")
            }
        }
    }



    messageDb.addValueEventListener(object : ValueEventListener{
        override fun onDataChange(snapshot: DataSnapshot) {
            val newData = mutableListOf<UserMessageData>()
        if (snapshot.exists()){
            for(user in snapshot.children){
                newData.add(UserMessageData(name =user.key.toString(), userId = user.key.toString(), time = ""))
                dataset = newData
            }
        }
        }

        override fun onCancelled(error: DatabaseError) {
            TODO("Not yet implemented")
        }

    })
  Scaffold(modifier = Modifier.fillMaxSize()) {
      Box(modifier = Modifier
          .fillMaxSize()
          .padding(it)){
      LazyColumn(modifier = Modifier.fillMaxSize()) {
          items(items = dataset ){ item ->
              UserMessageList(navController,uidViewModel, name = item.name, userId = item.userId, time =item.time)

          }

      }
          Button(
              modifier = Modifier
                  .width(300.dp)
                  .height(55.dp),
              shape = RoundedCornerShape(5.dp),
              colors = ButtonDefaults.buttonColors(containerColor = Color(0xff000000)),
              onClick = { authViewModel.Signout() }
          ) {
              Text(text = "Log Out")
          }
      }

  }
}
@Preview
@Composable
private fun LayoutPreview() {
    Admin(navController = rememberNavController(), viewModel(), viewModel())

}