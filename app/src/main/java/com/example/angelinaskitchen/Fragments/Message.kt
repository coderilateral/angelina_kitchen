package com.example.angelinaskitchen.Fragments

import AuthViewModel
import android.util.Log
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults.topAppBarColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

import androidx.compose.ui.graphics.Color

import androidx.compose.ui.res.painterResource

import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.angelinaskitchen.Models.UidViewModel
import com.example.angelinaskitchen.R
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.database

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Message(navController:NavController,uidViewModel: UidViewModel, authViewModel: AuthViewModel) {

    val database = FirebaseDatabase.getInstance()
    val messageDb = database.getReference("Messages")
    val auth: FirebaseAuth = FirebaseAuth.getInstance()
    val userRef = FirebaseDatabase.getInstance().getReference("UserInfo").child(auth.currentUser?.uid.toString())
    var dataset by remember { mutableStateOf(listOf<MessageData>()) }
    Log.d("ga lolo", uidViewModel.uid)
    messageDb.child(
        if (uidViewModel.uid.isEmpty()) {
            auth.currentUser?.uid.toString()
        } else {
            uidViewModel.uid
        }).addValueEventListener(object : ValueEventListener{
        override fun onDataChange(snapshot: DataSnapshot) {
            val newData = mutableListOf<MessageData>()
            if (snapshot.exists()){
                for (data in snapshot.children){
                    newData.add(MessageData(message = data.child("message").value.toString(),
                        recipient = data.child("recipient").value.toString()))
                    Log.d("sah",data.child("message").value.toString())
                    dataset = newData
                }
            }
        }

        override fun onCancelled(error: DatabaseError) {
            TODO("Not yet implemented")
        }

    })
    Scaffold(
        topBar = {
        TopAppBar(
            colors = topAppBarColors(
                containerColor = Color.White

            ),
            title = {
            },
            navigationIcon = {
                IconButton(onClick = { navController.navigate("screen_B") }) {
                    Icon( painter = painterResource(id = R.drawable.baseline_arrow_back_ios_new_24)
                        ,
                        contentDescription = "Localized description"
                    )
                }
            }
        )
    } ) { innerPadding ->
        Column(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxWidth()
                .fillMaxHeight()
        ) {
            Box(modifier = Modifier.height(500.dp)) {
                LazyColumn {

                    items(items = dataset) { item ->
                       MessageBubble(messageData = item, viewModel = authViewModel)

                    }
                }
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(70.dp)
                    .background(Color.White),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                var message by remember { mutableStateOf("") }
                OutlinedTextField(
                    modifier = Modifier.width(300.dp),
                    value = message,
                    onValueChange = { message = it },
                    label = { Text(text = "Type") },
                    colors = TextFieldDefaults.outlinedTextFieldColors(
                        unfocusedLabelColor = Color.Black,
                        focusedLabelColor = Color.Black,
                        unfocusedBorderColor = Color.Black,  // Setting the unfocused border color to black
                        focusedBorderColor = Color.Black     // Setting the focused border color to black
                    )
                )
                TextButton(onClick = {
                    userRef.addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onDataChange(snapshot: DataSnapshot) {
                            val status = snapshot.child("Status").getValue(String::class.java)

                            if (status == "0") {
                                messageDb.child(auth.currentUser?.uid.toString()).child(messageDb.push().key.toString())
                                    .apply {
                                        child("message").setValue(message)
                                        child("recipient").setValue(auth.currentUser?.uid.toString())
                                    }
                            } else if (status == "1") {
                                messageDb.child(uidViewModel.uid).child(messageDb.push().key.toString())
                                    .apply {
                                        child("message").setValue(message)
                                        child("recipient").setValue("admin")
                                    }
                            }
                        }

                        override fun onCancelled(error: DatabaseError) {

                        }
                    })

                }) {
                    Icon(modifier = Modifier.size(100.dp),
                        painter = painterResource(id = R.drawable.baseline_send_24),
                        contentDescription = ""
                    )
                }

            }
        }

    }
}
//fun DrawScope.drawTopAndBottomBorders() {
//    val strokeWidth = 1.dp.toPx()
//    val cardWidth = size.width
//    val cardHeight = size.height
//
//    // Draw top border
//    drawLine(
//        color = Color.Black,
//        start = Offset(0f, 0f),
//        end = Offset(cardWidth, 0f),
//        strokeWidth = strokeWidth
//    )
//
//    // Draw bottom border
//    drawLine(
//        color = Color.Black,
//        start = Offset(0f, cardHeight),
//        end = Offset(cardWidth, cardHeight),
//        strokeWidth = strokeWidth
//    )
//}

@Preview
@Composable
private fun LayoutPreview() {
    val navController = rememberNavController()
    Message(navController, viewModel(), viewModel())
    
}

//Spacer(modifier = Modifier.padding(100.dp))
//Text(
//"Layout offset modifier sample",
//Modifier
//.background(Color.Red)
//.wrapContentSize(Alignment.Center)
//.offset(10.dp, -20.dp)
//)
//
//Box(
//modifier = Modifier
//.size(width = 240.dp, height = 100.dp)
//.clip(MaterialTheme.shapes.medium)
//) {
//    Card(
//        colors = CardDefaults.cardColors(
//            containerColor = MaterialTheme.colorScheme.surface,
//        ),
//        modifier = Modifier.fillMaxSize()
//    ) {
//        Text(
//            text = "Outlined",
//            modifier = Modifier
//                .padding(16.dp),
//            textAlign = TextAlign.Center,
//        )
//    }
//    Canvas(
//        modifier = Modifier.fillMaxSize()
//    ) {
//        drawTopAndBottomBorders()
//    }
//}