package com.example.angelinaskitchen.Fragments

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.angelinaskitchen.R

@Composable
fun BottomNavbar(navController: NavController, page:String){
    Card(
        modifier = Modifier.fillMaxWidth().size(width = 0.dp, height = 80.dp),
        shape = RoundedCornerShape(topStart = 15.dp, topEnd = 15.dp, bottomEnd = 0.dp, bottomStart = 0.dp) // Set rounded corners
    ) {
        Row(modifier = Modifier.fillMaxWidth().background(Color.Black).size(width = 0.dp,height = 90.dp), horizontalArrangement = Arrangement.SpaceEvenly, verticalAlignment = Alignment.CenterVertically,) {
            IconButton(onClick = { navController.navigate("screen_B") }) {
                Image(painter = painterResource(id = if (page == "Home") {
                    R.drawable.whitehome
                } else {
                    R.drawable.whitehomeu
                }), contentDescription = "")
            }
            IconButton(onClick = { navController.navigate("screen_G") }) {
                Image(painter = painterResource(id =if (page == "Cart") {
                    R.drawable.white_cart_filled
                } else {
                    R.drawable.white_cart
                }), contentDescription = "")

            }
            IconButton(onClick = {  navController.navigate("screen_H")}) {
                Image(painter = painterResource(id = if(page == "Message"){
                    R.drawable.white_message_filled
                }else{
                    R.drawable.white_message
                }), contentDescription = "")

            }
            IconButton(onClick = {  navController.navigate("screen_F")}) {
                Image(painter = painterResource(id = if(page == "Profile") {
                    R.drawable.white_user_filled
                } else {
                    R.drawable.white_user
                }),contentDescription = "")

            }

        }
    }

}

@Preview(showBackground = true)
@Composable
private fun LayoutPreview() {
    val navController = rememberNavController()
    BottomNavbar(navController, "")
}