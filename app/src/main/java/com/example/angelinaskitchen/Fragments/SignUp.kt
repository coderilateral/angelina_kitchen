package com.example.angelinaskitchen.Fragments

import AuthViewModel
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CheckboxDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SignUp(navController: NavController, authViewModel: AuthViewModel) {
    var checked by remember { mutableStateOf(false) }
    var isDialogShow by remember { mutableStateOf(false) }
    var email by remember { mutableStateOf("") }
    var password by rememberSaveable { mutableStateOf("") }
    var username by rememberSaveable { mutableStateOf("") }
    var address by rememberSaveable { mutableStateOf("") }
    var emailError by remember { mutableStateOf("") }
    var passwordError by remember { mutableStateOf("") }
    var usernameError by remember { mutableStateOf("") }
    var addressError by remember { mutableStateOf("") }
    var phoneNumber by remember { mutableStateOf("+63") }
    var phoneNumberError by remember { mutableStateOf("") }
    if (isDialogShow) {
        TermsAndCondition { isDialogShow = !isDialogShow }
    }
    var showPassword by remember { mutableStateOf(false) }
    val passwordVisualTransformation = remember { PasswordVisualTransformation() }


    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .padding(vertical = 20.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.height(50.dp))
        Text(
            modifier = Modifier.padding(vertical = 5.dp),
            textAlign = TextAlign.Center,
            text = "Registration",
            fontWeight = FontWeight.SemiBold,
            fontSize = 28.sp
        )
        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxWidth()
                .padding(25.dp)
        )
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(10.dp) // Adjusted to align at the top without large spaces
        ) {
            // Email field
            OutlinedTextField(
                modifier = Modifier.width(350.dp),
                value = email,
                onValueChange = {
                    email = it
                    emailError = ""
                },
                label = { Text(text = "Email") },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    unfocusedLabelColor = Color.Black,
                    focusedLabelColor = Color.Black,
                    unfocusedBorderColor = Color.Black,
                    focusedBorderColor = Color.Black
                )
            )
            if (emailError.isNotEmpty()) {
                Text(text = emailError, color = Color.Red, fontSize = 12.sp)
            }

            // Password field
            OutlinedTextField(
                modifier = Modifier.width(350.dp),
                value = password,
                onValueChange = {
                    password = it
                    passwordError = ""
                },
                label = { Text(text = "Password") },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    unfocusedLabelColor = Color.Black,
                    focusedLabelColor = Color.Black,
                    unfocusedBorderColor = Color.Black,
                    focusedBorderColor = Color.Black
                ),
                visualTransformation = if (showPassword) {
                    VisualTransformation.None
                } else {
                    passwordVisualTransformation
                },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                trailingIcon = {
                    Icon(
                        imageVector = if (showPassword) {
                            Icons.Filled.Visibility
                        } else {
                            Icons.Filled.VisibilityOff
                        },
                        contentDescription = "Toggle password visibility",
                        modifier = Modifier.clickable { showPassword = !showPassword }
                    )
                }
            )
            if (passwordError.isNotEmpty()) {
                Text(text = passwordError, color = Color.Red, fontSize = 12.sp)
            }

            // Username field
            OutlinedTextField(
                modifier = Modifier.width(350.dp),
                value = username,
                onValueChange = {
                    username = it
                    usernameError = ""
                },
                label = { Text(text = "Username") },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    unfocusedLabelColor = Color.Black,
                    focusedLabelColor = Color.Black,
                    unfocusedBorderColor = Color.Black,
                    focusedBorderColor = Color.Black
                )
            )
            if (usernameError.isNotEmpty()) {
                Text(text = usernameError, color = Color.Red, fontSize = 12.sp)
            }

            // Address field
            OutlinedTextField(
                modifier = Modifier.width(350.dp),
                value = address,
                onValueChange = {
                    address = it
                    addressError = ""
                },
                label = { Text(text = "Address") },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    unfocusedLabelColor = Color.Black,
                    focusedLabelColor = Color.Black,
                    unfocusedBorderColor = Color.Black,
                    focusedBorderColor = Color.Black
                )
            )
            if (addressError.isNotEmpty()) {
                Text(text = addressError, color = Color.Red, fontSize = 12.sp)
            }

            // Phone number field
            OutlinedTextField(
                modifier = Modifier.width(350.dp),
                value = phoneNumber,
                onValueChange = { newValue ->
                    phoneNumberError = ""
                    if (!newValue.startsWith("+63")) {
                        phoneNumberError = "Phone number must start with +63"
                    }
                    if (newValue.length <= 13) {
                        phoneNumber = newValue
                    }
                },
                label = { Text(text = "Phone Number") },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    unfocusedLabelColor = Color.Black,
                    focusedLabelColor = Color.Black,
                    unfocusedBorderColor = Color.Black,
                    focusedBorderColor = Color.Black
                ),
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Phone)
            )
            if (phoneNumberError.isNotEmpty()) {
                Text(text = phoneNumberError, color = Color.Red, fontSize = 12.sp)
            }

            // Agreement row
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 35.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Start
            ) {
                Checkbox(
                    checked = checked,
                    onCheckedChange = { checked = it },
                    colors = CheckboxDefaults.colors(
                        checkedColor = Color.Black,
                        uncheckedColor = Color.Black,
                        checkmarkColor = Color.White
                    )
                )
                Text(text = "I Agree with", color = Color.Gray)
                TextButton(onClick = { isDialogShow = !isDialogShow }) {
                    Text(
                        modifier = Modifier.padding(5.dp),
                        textDecoration = TextDecoration.Underline,
                        text = "Terms and Conditions",
                        color = Color(0xfffa5305)
                    )
                }
            }

            Spacer(modifier = Modifier.padding(20.dp))

            // Submit button
            Button(
                modifier = Modifier.size(width = 350.dp, height = 55.dp),
                enabled = checked,
                shape = RoundedCornerShape(5.dp),
                colors = ButtonDefaults.buttonColors(containerColor = Color(0xff000000)),
                onClick = {
                    var isValid = true
                    if (email.isEmpty()) {
                        emailError = "Email cannot be empty"
                        isValid = false
                    }
                    if (password.isEmpty()) {
                        passwordError = "Password cannot be empty"
                        isValid = false
                    }
                    if (username.isEmpty()) {
                        usernameError = "Username cannot be empty"
                        isValid = false
                    }
                    if (address.isEmpty()) {
                        addressError = "Address cannot be empty"
                        isValid = false
                    }
                    if (phoneNumber.isEmpty()) {
                        phoneNumberError = "Phone Number cannot be empty"
                        isValid = false
                    }
                    if (phoneNumber.length != 13) {
                        phoneNumberError = "Incomplete phone number"
                        isValid = false
                    }
                    if (isValid) {
                        authViewModel.Signup(email, password, address, phoneNumber, username)
                         navController.navigate("screen_A")
                    }
                }
            ) {
                Text(text = "Submit")
            }
        }

    }

}



@Preview(showBackground = true)
@Composable
private fun LayoutPreview() {
    val navController = rememberNavController()
    SignUp(navController, AuthViewModel())
}