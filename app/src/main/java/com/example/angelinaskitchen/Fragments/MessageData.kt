package com.example.angelinaskitchen.Fragments

data class MessageData(
    val message : String = "",
    val recipient : String = ""
)
