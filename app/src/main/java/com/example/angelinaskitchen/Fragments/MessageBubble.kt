package com.example.angelinaskitchen.Fragments

import AuthViewModel
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel

@Composable
fun MessageBubble(messageData: MessageData, viewModel: AuthViewModel) {

    val gradient = if (messageData.recipient == "admin") {
        Brush.verticalGradient(colors = listOf(Color(0xFF4CAF50), Color(0xFF2E7D32))) // Admin gradient
    } else {
        Brush.verticalGradient(colors = listOf(Color(0xFF03A9F4), Color(0xFF0288D1))) // Receiver gradient
    }
        val width = 160.dp
    val bubbleModifier = Modifier
        .padding(3.dp)
        .background(gradient, shape = RoundedCornerShape(16.dp))
        .padding(10.dp)
        .widthIn(max=width)

    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement =
            if(viewModel.authState.value.toString() == "AdminAuthenticated"){
               if (messageData.recipient == "admin"){
                   Arrangement.End
               }else {
                   Arrangement.Start
               }
            }else {
                if (messageData.recipient != "admin"){
                    Arrangement.End
                }else {
                    Arrangement.Start
                }
            }

    ) {
        Box(modifier = bubbleModifier) {
            Text(
            text = messageData.message,
            color = Color.White
        )

        }
    }
}

@Preview (showBackground = true)
@Composable
private fun LayOutPreview() {
    MessageBubble(messageData = MessageData(), viewModel())
    
}