package com.example.angelinaskitchen.Fragments

import android.util.Log
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import coil.compose.AsyncImage
import com.example.angelinaskitchen.Models.SharedViewModel
import com.example.angelinaskitchen.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

@Composable
fun ItemPreview(navController: NavController, viewModel: SharedViewModel) {
    val database = FirebaseDatabase.getInstance()
    val myRef = database.getReference("Cart")
    val auth = FirebaseAuth.getInstance()
    val userId = auth.currentUser?.uid ?: ""
    var productId by remember {
        mutableStateOf("")
    }
    var image by remember { mutableStateOf("") }
    var productName by remember { mutableStateOf("") }
    var productDescription by remember { mutableStateOf("") }
    var itemCount by remember { mutableStateOf(0) }
    var productPrice by remember { mutableIntStateOf(0) }
    var price by remember { mutableStateOf(0) }

    // Load the image URL
    image = imageBuilder(viewModel.id.toString())

    // Fetch product details from the menu
    FirebaseDatabase.getInstance().getReference("menu/${viewModel.id}")
        .addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                productId = viewModel.id.toString()
                productName = snapshot.child("name").value as String
                productPrice = snapshot.child("price").value.toString().toInt()
                price = productPrice
                productDescription = snapshot.child("description").getValue(String::class.java) ?: ""

                // Fetch quantity from cart
                myRef.child(userId).child(productId).addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(cartSnapshot: DataSnapshot) {
                        itemCount = cartSnapshot.child("quantity").getValue(Int::class.java) ?: 0
                        price = itemCount * productPrice
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Log.e("DatabaseError", error.message)
                    }
                })
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("DatabaseError", error.message)
            }
        })

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        bottomBar = {
            Column(
                modifier = Modifier.fillMaxWidth(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Button(
                    modifier = Modifier.size(width = 300.dp, height = 55.dp),
                    shape = RoundedCornerShape(5.dp),
                    colors = ButtonDefaults.buttonColors(
                        containerColor = Color(0xff000000)
                    ),
                    onClick = {
                        // Handle checkout logic here
                        myRef.child(userId).child(productName).addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onDataChange(snapshot: DataSnapshot) {
                                myRef.child(userId).child(viewModel.id.toString()).child("quantity").setValue(itemCount)
                            }

                            override fun onCancelled(error: DatabaseError) {
                                Log.e("DatabaseError", error.message)
                            }
                        })
                        navController.navigate("screen_G")
                    }
                ) {
                    Text(text = "Checkout")
                }
            }
        }
    ) { innerPadding ->
        Column(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxWidth()
                .fillMaxHeight(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp)
            ) {
                ImageView(image)
                IconButton(
                    modifier = Modifier
                        .align(Alignment.TopStart)
                        .padding(3.dp)
                        .size(30.dp),
                    onClick = { navController.navigate("screen_D") }
                ) {
                    Icon(painter = painterResource(id = R.drawable.baseline_close_24), contentDescription = "")
                }
            }
            Spacer(modifier = Modifier.padding(vertical = 10.dp))
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp)
            ) {
                Text(text = productName, fontWeight = FontWeight.SemiBold, fontSize = 22.sp)
                Text(
                    modifier = Modifier.padding(vertical = 10.dp),
                    text = "Chicharon Bulaklak or deep-fried ruffled fat is a popular Filipino appetizer",
                    color = Color.Gray,
                    fontWeight = FontWeight.SemiBold
                )
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 25.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    OutlinedCard(
                        colors = CardDefaults.cardColors(containerColor = MaterialTheme.colorScheme.surface),
                        border = BorderStroke(0.1.dp, Color.Black),
                        modifier = Modifier.size(width = 120.dp, height = 45.dp)
                    ) {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.SpaceEvenly
                        ) {
                            TextButton(
                                modifier = Modifier.size(45.dp),
                                colors = ButtonDefaults.buttonColors(
                                    containerColor = Color(0xffFFFFFFF), contentColor = Color.Black
                                ),
                                onClick = {
                                    if (itemCount > 0) {
                                        itemCount--
                                        price = itemCount * productPrice

                                        if (itemCount == 0){
                                            myRef.child(userId).child(viewModel.id.toString()).removeValue()
                                        }else {
                                            myRef.child(userId).child(viewModel.id.toString()).child("quantity").setValue(itemCount)
                                        }
                                    }
                                }
                            ) {
                                Icon(painter = painterResource(id = R.drawable.baseline_minus_24), contentDescription = "")
                            }
                            Text(text = itemCount.toString(), fontSize = 18.sp)
                            TextButton(
                                modifier = Modifier.size(45.dp),
                                colors = ButtonDefaults.buttonColors(
                                    containerColor = Color(0xffFFFFFFF), contentColor = Color.Black
                                ),
                                onClick = {
                                    itemCount++
                                    price = itemCount * productPrice
                                    // Update the quantity in Firebase
                                    myRef.child(userId).child(viewModel.id.toString()).child("quantity").setValue(itemCount)
                                }
                            ) {
                                Icon(painter = painterResource(id = R.drawable.baseline_add_24), contentDescription = "")
                            }
                        }
                    }
                    Text(
                        modifier = Modifier.padding(horizontal = 15.dp),
                        text = "\$$price",
                        fontSize = 22.sp,
                        fontWeight = FontWeight.SemiBold
                    )
                }
                Text(text = "About Offers", fontWeight = FontWeight.SemiBold, fontSize = 17.sp)
                Spacer(modifier = Modifier.padding(vertical = 10.dp))
                Text(color = Color.Gray, text = productDescription, fontSize = 22.sp)
            }
        }
    }
}

@Composable
fun ImageView(image: String) {
    AsyncImage(
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)
            .size(360.dp),
        model = image,
        contentDescription = "",
        contentScale = ContentScale.Crop
    )
}

fun imageBuilder(id: String): String {
    return "https://firebasestorage.googleapis.com/v0/b/angelina-s-kitchen.appspot.com/o/menu%2F$id.png?alt=media"
}

@Preview(showBackground = true)
@Composable
private fun LayoutPreview() {
    val navController = rememberNavController()
    ItemPreview(navController, viewModel())
}
