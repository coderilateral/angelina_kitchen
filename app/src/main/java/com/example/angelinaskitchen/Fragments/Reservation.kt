package com.example.angelinaskitchen.Fragments

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun Reservation(){
    Scaffold {innerPadding->
        Column(modifier = Modifier
            .padding(innerPadding)
            .fillMaxWidth()
            .fillMaxHeight(), horizontalAlignment = Alignment.CenterHorizontally) {

            Column (modifier = Modifier
                .padding(20.dp)){
            Text(text = "Food Reservation", fontWeight = FontWeight.SemiBold, fontSize = 22.sp, textAlign = TextAlign.Center)
            }
            Column (modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 15.dp)
                .verticalScroll(
                    rememberScrollState()
                ), horizontalAlignment = Alignment.CenterHorizontally){
                Spacer(modifier = Modifier.height(20.dp))
                Column(modifier = Modifier.fillMaxWidth().padding(vertical = 8.dp)) {
                    Text(text = "Date", fontSize = 18.sp,)
                }
                ElevatedCard(modifier = Modifier.size(width = 360.dp, height = 300.dp),elevation = CardDefaults.cardElevation(defaultElevation = 4.dp), onClick = { /*TODO*/ }) {}
                Spacer(modifier = Modifier.height(20.dp))
                Column(modifier = Modifier.fillMaxWidth().padding(vertical = 8.dp)) {
                    Text(text = "Time", fontSize = 18.sp,)
                }
                ElevatedCard(modifier = Modifier.size(width = 360.dp, height = 300.dp),elevation = CardDefaults.cardElevation(defaultElevation =4.dp), onClick = { /*TODO*/ }) {}

            }
        }

    }
}

@Preview (showBackground = true)
@Composable
private fun LayoutPreview() {
    Reservation()
    
}