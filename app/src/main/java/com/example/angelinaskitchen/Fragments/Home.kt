package com.example.angelinaskitchen.Fragments

import AuthViewModel
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.angelinaskitchen.Models.SharedViewModel
import com.example.angelinaskitchen.R


@Composable
fun Home(navController: NavController, viewModel: SharedViewModel, authViewModel:AuthViewModel) {
    val authState = authViewModel.authState.observeAsState()
    LaunchedEffect(authState.value) {
        when(authState.value){
            is AuthState.Unauthenticated -> navController.navigate("screen_A")
            else -> Unit
        }
    }
    Scaffold(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()

    , bottomBar ={  BottomNavbar(navController, "Home") }) { innerPadding ->
        Column(
            modifier = Modifier
                .padding(vertical = 30.dp)
                .padding(innerPadding)
                .fillMaxWidth()
                .fillMaxHeight(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "Categories",
                fontWeight = FontWeight.SemiBold,
                fontSize = 22.sp,
                textAlign = TextAlign.Center
            )
            Spacer(modifier = Modifier.height(45.dp))
            Column(
                modifier = Modifier
                    .fillMaxHeight()
                    .padding(vertical = 18.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.spacedBy(30.dp)
            ) {
                LazyVerticalGrid(
                    columns = GridCells.Fixed(2),
                    verticalArrangement = Arrangement.spacedBy(20.dp), horizontalArrangement = Arrangement.Center
                ) {
                    items(5) { index ->
                        when (index) {
                            0 -> card(navController, R.drawable.fried_rice, "Vegetables", "vegetables", viewModel)
                            1 -> card(navController, R.drawable.cocktail, "Drinks", "drinks", viewModel)
                            2 -> card(navController, R.drawable.meat, "Meat", "meat", viewModel)
                            3 -> card(navController, R.drawable.seafood, "Seafood", "seafoods", viewModel)
                            4->  card(navController, R.drawable.dinner, "Meals", "meals", viewModel)
                        }
                    }
                }


//                Column(
//                    modifier = Modifier
//                        .fillMaxHeight()
//                        .padding(vertical = 30.dp)
//                ) {
//
//
//                    Button(modifier = Modifier.size(width = 300.dp, height = 55.dp),
//                        shape = RoundedCornerShape(5.dp),
//                        colors = ButtonDefaults.buttonColors(
//                            containerColor = Color(0xff000000)
//                        ),
//                        onClick = {
//                            navController.navigate("screen_A")
//
//                        }) {
//                        Text(text = "Screen A")
//
//                    }
//
//
//                }
            }

        }

    }
}

@Composable
fun card(navController: NavController,logo:Int,label1:String, id1:String, viewModel: SharedViewModel){


    Row(
        horizontalArrangement = Arrangement.SpaceEvenly,
        modifier = Modifier.fillMaxWidth()
    ) {
        Card(elevation = CardDefaults.cardElevation(defaultElevation = 0.5.dp
        ),modifier = Modifier
                .size(width = 140.dp, height = 165.dp), colors = CardDefaults.cardColors(containerColor = Color.White),
            onClick = {
                viewModel.type = id1
                navController.navigate("screen_D")
            }
        ) {
           Column (modifier = Modifier
               .fillMaxWidth()
               .padding(20.dp)
               .fillMaxHeight(), horizontalAlignment = Alignment.CenterHorizontally
           , verticalArrangement = Arrangement.Center){

               Image(painter = painterResource(id = logo), contentDescription = "")
               Spacer(modifier = Modifier.padding(5.dp))
               Text(text = label1)
           }

        }

    }
}
@Preview(showBackground = true)
@Composable
private fun AppPreview() {
    val navController = rememberNavController()
    Home(navController, viewModel(), AuthViewModel())

}