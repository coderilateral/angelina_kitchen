package com.example.angelinaskitchen.Fragments


import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.example.angelinaskitchen.Models.CartData
import com.example.angelinaskitchen.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase


@Composable
fun CartCard(cartData: CartData, onQuantityChange : (CartData)-> Unit){
    val database = FirebaseDatabase.getInstance()
    val myRef = database.getReference("Cart")
    val auth = FirebaseAuth.getInstance()
    val userId = auth.currentUser?.uid ?: ""

    var itemCount by remember { mutableIntStateOf(cartData.quantity) }
    var productPrice by remember { mutableIntStateOf(cartData.price) }
    var price by remember { mutableIntStateOf(itemCount * productPrice) }


    ElevatedCard( elevation = CardDefaults.cardElevation(defaultElevation = 3.dp),
        colors = CardDefaults.cardColors(
            containerColor = Color(
                when (cartData.type) {
                    "Meat" -> {
                        0xFFffe4e4
                    }
                    "Seafoods" -> {
                        0xFFCDF5FD
                    }
                    "Drinks" -> {
                        0xfffeeeff
                    }
                    "Vegetables" -> {
                        0xffecfee4
                    }
                    "Meals" -> {
                        0xffffdabc

                    }
                    else -> {
                        0xffffffff
                    }
                }
            ),
        ),
        modifier = Modifier
            .size(width = 360.dp,height = 108.dp),onClick = { /*TODO*/ }) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(),verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(10.dp)) {
            CartImage(cartData = cartData)
            Column(modifier = Modifier
                .fillMaxHeight()
                .padding(20.dp), verticalArrangement = Arrangement.SpaceEvenly, horizontalAlignment = Alignment.CenterHorizontally){
                   Text(fontWeight = FontWeight.Bold,fontSize = 16.sp,text = cartData.name,overflow = TextOverflow.Ellipsis)
                    Spacer(modifier = Modifier.height(12.dp))
                  Row (modifier = Modifier, verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(5.dp)){
                      IconButton(modifier = Modifier, onClick = {

                          if (itemCount > 0) {
                              itemCount--
                              price = itemCount * productPrice
                              if (itemCount == 0){
                                  myRef.child(userId).child(cartData.id).removeValue()
                                  onQuantityChange(cartData)
                              }else {
                                  myRef.child(userId).child(cartData.id).child("quantity").setValue(itemCount)
                              }
                          }
                      }) { Icon(modifier = Modifier.size(16.dp), painter = painterResource(id = R.drawable.baseline_minus_24), contentDescription = "")}
                      Text(text =cartData.quantity.toString(), fontSize = 16.sp)
                      IconButton(onClick = {
                          itemCount++
                          price = itemCount * productPrice
                          // Update the quantity in Firebase
                          myRef.child(userId).child(cartData.id).child("quantity").setValue(itemCount)

                      }) { Icon(modifier = Modifier.size(16.dp),painter = painterResource(id = R.drawable.baseline_add_24), contentDescription = "")}
                  }


            }

           Column (modifier = Modifier
               .fillMaxWidth()
               .fillMaxHeight(), verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally){
               IconButton(onClick = { /*TODO*/ }) {
                   Icon(modifier =Modifier.size(26.dp),painter = painterResource(id = R.drawable.delete), contentDescription = "")}
               Text(fontSize = 16.sp,text = price.toString())
           }
        }

    }
}


@Composable
private fun CartImage(cartData: CartData){
    AsyncImage( model = cartData.image, contentDescription = "", modifier = Modifier
        .padding(5.dp)
        .size(100.dp), contentScale = ContentScale.Crop)
}
//@Preview
//@Composable
//private fun LayoutPreview() {
//    CartCard(cartData = CartData(   id = "",
//        name ="Freelan",
//        price = 0,
//        image = R.drawable.cocktail
//        , type = "", quantity = 0))
//
//}