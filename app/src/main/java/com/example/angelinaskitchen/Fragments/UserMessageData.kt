package com.example.angelinaskitchen.Fragments

data class UserMessageData(
    val name: String = "",
    val userId : String = "",
    val time : String = ""

)
