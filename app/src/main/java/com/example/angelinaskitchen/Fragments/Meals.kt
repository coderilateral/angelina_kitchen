package com.example.angelinaskitchen.Fragments

import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.angelinaskitchen.Models.Items
import com.example.angelinaskitchen.Models.SharedViewModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


@Composable
fun Meals(navController: NavController, viewModel: SharedViewModel) {
    val type: String = viewModel.type.toString()

    var dataset by remember { mutableStateOf(listOf<Items>()) }

    val database = FirebaseDatabase.getInstance().getReference("menu")

    database.addValueEventListener(object : ValueEventListener{
        override fun onDataChange(snapshot: DataSnapshot) {
            val newDataset = mutableListOf<Items>()
            for (data in snapshot.children) {
                if (type != (data.child("type").getValue(String::class.java) ?: "")) continue
                val name = data.child("name").getValue(String::class.java) ?: ""
                val price = data.child("price").getValue(Int::class.java) ?: 0
                val imageUrl = urlBuilder(data.key.toString())
                newDataset.add(Items(data.key.toString(),name, price,imageUrl))
                dataset = newDataset
            }
        }

        override fun onCancelled(error: DatabaseError) {
            Log.e("Meals", "Database error: ${error.message}")
        }
    })

   Scaffold (modifier = Modifier){innerPadding->
       Column(modifier = Modifier
           .padding(innerPadding)
           .padding(10.dp)
           .fillMaxWidth()
           .fillMaxHeight(), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center) {

          Column (modifier = Modifier.padding(20.dp)){
              Text(
                  text = "Meals",
                  fontWeight = FontWeight.SemiBold,
                  fontSize = 22.sp,
                  textAlign = TextAlign.Center
              )

          }

           LazyVerticalGrid(
               modifier = Modifier
                   .fillMaxWidth()
                   .fillMaxHeight().padding(10.dp),
               columns = GridCells.Fixed(2),
               verticalArrangement = Arrangement.spacedBy(16.dp),
               horizontalArrangement = Arrangement.spacedBy(16.dp)
           ) {
               items(items = dataset) { item ->
                   ItemsCard(item,viewModel,navController)
               }
           }

       }

   }
}
fun urlBuilder(id: String): String{
    return "https://firebasestorage.googleapis.com/v0/b/angelina-s-kitchen.appspot.com/o/menu%2F$id.png?alt=media"
}

class Utils{
    fun urlBuilder(id: String): String{
        return "https://firebasestorage.googleapis.com/v0/b/angelina-s-kitchen.appspot.com/o/menu%2F$id.png?alt=media"
    }
}

@Preview (showBackground = true)
@Composable
private fun LayoutPreview() {
    val navController = rememberNavController()
    Meals(navController, viewModel())
    
}