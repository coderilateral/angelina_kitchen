package com.example.angelinaskitchen.Fragments

import AuthViewModel
import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.angelinaskitchen.R




@Composable
fun Profile(navController: NavController, authViewModel: AuthViewModel) {
    val authState by authViewModel.authState.observeAsState()

    LaunchedEffect(authState) {
        when (authState) {
            is AuthState.Unauthenticated -> {
                Log.d("ProfileScreen", "Navigating to screen_A")
                navController.navigate("screen_A") {
                    popUpTo(navController.graph.startDestinationId) { inclusive = true }
                }
            }

            is AuthState.Authenticated -> {
                Log.d("ProfileScreen", "User is authenticated as client")
                // Here you can handle what happens when the client is authenticated
                // For instance, you can load the profile data or update UI accordingly
            }

            is AuthState.Error -> {
                val errorMessage = (authState as AuthState.Error).message
                Log.d("ProfileScreen", "Authentication error: $errorMessage")
//                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
            }

            else -> {
                Log.d("ProfileScreen", "Unexpected authState: $authState")
            }
        }
    }


        Scaffold(bottomBar = { }) { innerPadding ->
        Column(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxWidth()
                .fillMaxHeight(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(modifier = Modifier.height(60.dp))
            ElevatedCard(
                modifier = Modifier.size(200.dp),
                elevation = CardDefaults.cardElevation(defaultElevation = 5.dp),
                colors = CardDefaults.cardColors(containerColor = Color(0xffebeeee)),
                shape = RoundedCornerShape(50)
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxSize(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Image(
                        modifier = Modifier
                            .size(180.dp)
                            .clip(CircleShape),
                        painter = painterResource(id = R.drawable.dwag),
                        contentDescription = null,
                        contentScale = ContentScale.Crop
                    )
                }
            }
            Spacer(modifier = Modifier.height(10.dp))
            Text(text = "Jane Doe", fontSize = 22.sp, fontWeight = FontWeight.SemiBold)
            Row(
                modifier = Modifier
                    .size(260.dp, 40.dp)
                    .padding(5.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                Image(modifier = Modifier.size(25.dp), painter = painterResource(id = R.drawable.pin), contentDescription = "")
                Spacer(modifier = Modifier.width(5.dp))
                Text(
                    text = "1234 Mabuhay Street Barangay",
                    textAlign = TextAlign.Center,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    fontSize = 12.sp,
                    fontWeight = FontWeight.SemiBold,
                    color = Color(0xff008000)
                )
            }
            Spacer(modifier = Modifier.height(20.dp))
            Column(modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 15.dp)
                .verticalScroll(
                    rememberScrollState()
                ), horizontalAlignment = Alignment.CenterHorizontally) {
                CardButton(label = "History", icon = R.drawable.profile_history,Color(0XffF4E6FD) )
                CardButton(label = "Cart", icon = R.drawable.profile_food_cart,Color(0xffE0F0FD) )
                CardButton(label = "Profile Settings", icon = R.drawable.profile_settings,Color(0xffFDEEEE) )


                Spacer(modifier = Modifier.height(30.dp))
                    Button(
                        modifier = Modifier
                            .width(300.dp)
                            .height(55.dp),
                        shape = RoundedCornerShape(5.dp),
                        colors = ButtonDefaults.buttonColors(containerColor = Color(0xff000000)),
                        onClick = { authViewModel.Signout() }
                    ) {
                        Text(text = "Log Out")
                    }
                Spacer(modifier = Modifier.height(50.dp))

            }
        }
    }
}

@Composable
fun CardButton(label:String ,icon:Int,cardColor:Color){
    Spacer(modifier = Modifier.padding(vertical = 5.dp))
    ElevatedCard(   elevation = CardDefaults.cardElevation(
        defaultElevation = 1.2.dp
    ),
        modifier = Modifier
            .size(width = 390.dp, height = 80.dp) ,colors = CardDefaults.cardColors(containerColor = Color(0xffffffff))
    ) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .padding(horizontal = 10.dp), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.SpaceBetween) {
            Row (verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(10.dp)){
                Card(modifier = Modifier.size(60.dp),colors =CardDefaults.cardColors(containerColor = cardColor)) {
                    Column(modifier = Modifier.fillMaxSize(), verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
                        Image(modifier = Modifier.size(40.dp), painter = painterResource(id = icon), contentDescription = "")

                    }
                }
                Text(text = label,)
            }
            Icon(painter = painterResource(id = R.drawable.baseline_arrow_forward_24), contentDescription = "")
        }
    }
}


@Preview(showBackground = true)
@Composable
private fun LayoutPreview() {
    val navController = rememberNavController()
    Profile(navController,AuthViewModel())
}
