package com.example.angelinaskitchen.Fragments

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.verticalScroll
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.text.font.FontWeight

@Composable
fun TermsAndCondition(onDismissRequest: () -> Unit) {
    Dialog(onDismissRequest = { onDismissRequest() }) {
        Card(
            modifier = Modifier.fillMaxWidth()
                .heightIn(max = 700.dp)
                .padding(20.dp),
            shape = RoundedCornerShape(16.dp), colors = CardDefaults.cardColors(containerColor = MaterialTheme.colorScheme.surface
            )) {
            Box(
                modifier = Modifier
                    .fillMaxSize()
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .verticalScroll(rememberScrollState())
                        .padding(16.dp)
                ) {
                    val termsContent = listOf(
                        "Terms and Conditions" to FontWeight.Bold,
                        "Effective Date: [Insert Date]" to FontWeight.Normal,
                        "Welcome to Angelina's Kitchen!" to FontWeight.Normal,
                        "These Terms and Conditions (\"Terms\") govern your use of the Angelina's Kitchen mobile application (the \"App\") and any services provided through the App. By accessing or using our App, you agree to be bound by these Terms. If you do not agree to these Terms, please do not use the App." to FontWeight.Normal,
                        "1. Acceptance of Terms" to FontWeight.Bold,
                        "By using Angelina's Kitchen, you acknowledge that you have read, understood, and agree to be bound by these Terms and our Privacy Policy. We may update these Terms from time to time, and your continued use of the App constitutes acceptance of any changes." to FontWeight.Normal,
                        "2. Eligibility" to FontWeight.Bold,
                        "You must be at least 18 years old to use this App. By using the App, you represent and warrant that you are at least 18 years old and have the legal capacity to enter into this agreement." to FontWeight.Normal,
                        "3. Account Registration" to FontWeight.Bold,
                        "To use certain features of the App, you may need to create an account. You agree to provide accurate, current, and complete information during registration and to update such information to keep it accurate, current, and complete. You are responsible for safeguarding your account information and for all activities that occur under your account." to FontWeight.Normal,
                        "4. Ordering" to FontWeight.Bold,
                        "- Ordering: You may place orders for food and beverages through the App. All orders are subject to availability and confirmation." to FontWeight.Normal,
                        "5.Pickup" to FontWeight.Bold,
                        "- Pickup: If you choose to pick up your order, you must do so within the time specified in the App. Failure to pick up your order may result in its cancellation." to FontWeight.Normal,
                        "- Order Cancellation: You may cancel your order before it is prepared. Once an order is in preparation, cancellation may not be possible." to FontWeight.Normal,
                        "- Refunds: Refunds are subject to our refund policy, which is available on the App. Any refunds will be processed according to the method of payment used for the original order." to FontWeight.Normal,
                        "6. User Conduct" to FontWeight.Bold,
                        "You agree not to use the App for any unlawful or prohibited purpose. You will not engage in any activity that interferes with or disrupts the App or its services. You are responsible for any content you submit to the App and must ensure that it complies with applicable laws." to FontWeight.Normal,
                        "7. Intellectual Property" to FontWeight.Bold,
                        "All content and materials available through the App, including text, graphics, logos, and images, are the property of Angelina's Kitchen or its licensors and are protected by intellectual property laws. You may not use, reproduce, or distribute any content from the App without our prior written consent." to FontWeight.Normal,
                        "8. Limitation of Liability" to FontWeight.Bold,
                        "To the fullest extent permitted by law, Angelina's Kitchen is not liable for any indirect, incidental, consequential, or punitive damages arising out of or in connection with your use of the App. Our liability for any direct damages is limited to the amount paid by you for the order giving rise to the claim." to FontWeight.Normal,
                        "9. Indemnification" to FontWeight.Bold,
                        "You agree to indemnify and hold harmless Angelina's Kitchen, its affiliates, officers, employees, and agents from any claims, liabilities, damages, losses, or expenses arising out of your use of the App or any violation of these Terms." to FontWeight.Normal,
                        "10. Termination" to FontWeight.Bold,
                        "We reserve the right to terminate or suspend your access to the App at any time, with or without cause, and with or without notice." to FontWeight.Normal,
                        "11. Governing Law" to FontWeight.Bold,
                        "These Terms are governed by and construed in accordance with the laws of [Your Jurisdiction]. Any disputes arising from these Terms or your use of the App will be resolved in the courts located in [Your Jurisdiction]." to FontWeight.Normal,
                        "12. Contact Us" to FontWeight.Bold,
                        "If you have any questions or concerns about these Terms or the App, please contact us at:" to FontWeight.Normal,
                        "Angelina's Kitchen" to FontWeight.Normal,
                        "[Your Contact Information]" to FontWeight.Normal,
                        "[Your Email Address]" to FontWeight.Normal,
                        "[Your Phone Number]" to FontWeight.Normal,
                        "13. Severability" to FontWeight.Bold,
                        "If any provision of these Terms is found to be invalid or unenforceable, the remaining provisions will continue to be valid and enforceable." to FontWeight.Normal
                    )

                    termsContent.forEach { (text, fontWeight) ->
                        Text(
                            text = text,
                            fontWeight = fontWeight,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(bottom = 8.dp),
                            textAlign = TextAlign.Left
                        )
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun PreviewTermsAndCondition() {
    TermsAndCondition(onDismissRequest = {})
}

