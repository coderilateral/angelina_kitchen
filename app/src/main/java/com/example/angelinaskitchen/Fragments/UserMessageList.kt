package com.example.angelinaskitchen.Fragments

import android.text.Layout
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.angelinaskitchen.Models.UidViewModel

@Composable
fun UserMessageList (navController: NavController,viewModel: UidViewModel,name:String,userId:String,time:String){
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .height(40.dp)
            .background(Color.Red)
            .clickable {
                viewModel.uid = userId
                navController.navigate("screen_H")
                // Handle the click event here
                // For example, you could navigate to another screen or show a dialog
            }
    ) {
        Row(
            modifier = Modifier.fillMaxSize(),
            horizontalArrangement = Arrangement.spacedBy(10.dp)
        ) {
            Text(text = name)
            Text(text = userId)
            Text(text = time)
        }
    }

}
@Preview(showBackground = true)
@Composable
private fun LayoutPreview() {
    UserMessageList(navController = rememberNavController(), viewModel(),"313123","1321313","1331323")

}