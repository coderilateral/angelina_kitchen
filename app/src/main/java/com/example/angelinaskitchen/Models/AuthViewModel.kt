import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.database

class AuthViewModel : ViewModel() {
    private lateinit var userId : String
    private val auth: FirebaseAuth = FirebaseAuth.getInstance()
    private val _authState = MutableLiveData<AuthState>()
    val authState: LiveData<AuthState> = _authState
    val database = Firebase.database
    val myRef = database.getReference("UserInfo")

    init {
        checkAuthStatus()
    }

//    private fun checkAuthStatus() {
//        if (auth.currentUser == null) {
//            _authState.value = AuthState.Unauthenticated
//        } else {
//            _authState.value = AuthState.Authenticated
//        }
//    }
private fun checkAuthStatus() {
    auth.addAuthStateListener { auth ->
        if (auth.currentUser == null) {
            _authState.value = AuthState.Unauthenticated
        } else {

            val currentUser = auth.currentUser
            if (currentUser != null) {
                val userRef = myRef.child(currentUser.uid)
                userRef.addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val status = snapshot.child("Status").value.toString()
                        when (status) {
                            "1" -> _authState.value = AuthState.AdminAuthenticated
                            "0" -> _authState.value = AuthState.Authenticated
                            else -> _authState.value = AuthState.Error("Unexpected status value")
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        _authState.value = AuthState.Error("Failed to fetch user data")
                    }
                })
            }
        }
    }
}

    fun Login(email: String, password: String,) {
        if (email.isEmpty() || password.isEmpty()) {
            _authState.value = AuthState.Error("Email or Password can't be empty")
            return
        }

        _authState.value = AuthState.Loading
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d("AuthViewModel", "Login successful")
                    val currentUser = auth.currentUser
                    if(currentUser != null){
                        val userRef = FirebaseDatabase.getInstance().getReference("UserInfo").child(currentUser.uid)
                        userRef.addListenerForSingleValueEvent(object : ValueEventListener{
                            override fun onDataChange(snapshot: DataSnapshot) {
                                val status = snapshot.child("Status").getValue(String::class.java)
                                if (status == "1"){
                                    _authState.value = AuthState.AdminAuthenticated
                                }else if (status == "0"){
                                    _authState.value = AuthState.Authenticated

                                }
                            }

                            override fun onCancelled(error: DatabaseError) {
                                TODO("Not yet implemented")
                            }

                        })
                    }


                } else {
                    val errorMessage = task.exception?.message ?: "Something went wrong"
                    Log.d("AuthViewModel", "Login failed: $errorMessage")
                    _authState.value = AuthState.Error(errorMessage)
                }
            }
            .addOnFailureListener { exception ->
                val errorMessage = exception.message ?: "Something went wrong"
                Log.d("AuthViewModel", "Login failure: $errorMessage")
                _authState.value = AuthState.Error(errorMessage)
            }



    }

    fun Signup(email: String, password: String,address:String,phoneNumber:String,username:String) {
        when {
            email.isEmpty() -> {
                _authState.value = AuthState.Error("Email can't be empty")
                return
            }
            username.isEmpty() -> {
                _authState.value = AuthState.Error("Username can't be empty")
                return
            }
            address.isEmpty() -> {
                _authState.value = AuthState.Error("Address can't be empty")
                return
            }
            phoneNumber.isEmpty() -> {
                _authState.value = AuthState.Error("Phone Number can't be empty")
                return
            }
            password.isEmpty() -> {
                _authState.value = AuthState.Error("Password can't be empty")
                return
            }
            else -> {
                // Handle successful validation here
                _authState.value = AuthState.Success // or whatever success state you use
            }
        }

        _authState.value = AuthState.Loading
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val user = Firebase.auth.currentUser
                    user?.let{
                        userId = user.uid}
                    myRef.child(userId).child("Address").setValue(address)
                    myRef.child(userId).child("PhoneNumber").setValue(phoneNumber)
                    myRef.child(userId).child("UserName").setValue(username)
                    myRef.child(userId).child("Status").setValue("0")

                        Log.d("AuthViewModel", "Signup successful")
                    _authState.value = AuthState.Authenticated
                } else {
                    val errorMessage = task.exception?.message ?: "Something went wrong"
                    Log.d("AuthViewModel", "Signup failed: $errorMessage")
                    _authState.value = AuthState.Error(errorMessage)
                }
            }
            .addOnFailureListener { exception ->
                val errorMessage = exception.message ?: "Something went wrong"
                Log.d("AuthViewModel", "Signup failure: $errorMessage")
                _authState.value = AuthState.Error(errorMessage)
            }
    }

    fun Signout() {
        auth.signOut()
        _authState.value = AuthState.Unauthenticated
    }
}

sealed class AuthState {
    data object Authenticated : AuthState()
    data object Unauthenticated : AuthState()
    data object Loading : AuthState()
    data object Success : AuthState()
    data class Error(val message: String) : AuthState()
    data object AdminAuthenticated : AuthState()

}
