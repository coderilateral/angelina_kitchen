package com.example.angelinaskitchen.Models

data class UsersChannel(
    val id: String ="",
    val name : String,
    val createdAt: Long = System.currentTimeMillis()
)
