    package com.example.angelinaskitchen.Models

    import androidx.lifecycle.ViewModel

    open class SharedViewModel : ViewModel() {
        var type: String? = ""
        var id :String? = ""
    }