package com.example.angelinaskitchen.Models
//
//import androidx.lifecycle.ViewModel
//import com.google.firebase.Firebase
//import com.google.firebase.database.database
//import dagger.hilt.android.lifecycle.HiltViewModel
//import kotlinx.coroutines.channels.Channel
//import kotlinx.coroutines.flow.MutableStateFlow
//import kotlinx.coroutines.flow.asStateFlow
//import javax.inject.Inject
//
//class ChatViewModel @Inject constructor(): ViewModel() {
//
//    private val firebaseDatabase = Firebase.database
//    private  val _channels = MutableStateFlow<List<UsersChannel>>(emptyList())
//    val channels = _channels.asStateFlow()
//    init {
//        getChannels()
//    }
//
//    private fun getChannels() {
//        firebaseDatabase.getReference("channel").get().addOnSuccessListener {
//            val list  = mutableListOf<UsersChannel>()
//            it.children.forEach{data ->
//                val channel = UsersChannel(data.key!!,data.value.toString())
//                list.add(channel)
//            }
//            _channels.value = list
//
//        }
//    }
//
//}