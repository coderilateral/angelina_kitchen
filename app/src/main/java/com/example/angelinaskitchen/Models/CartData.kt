package com.example.angelinaskitchen.Models

data class CartData(
    val id: String,
    val name: String,
    val price: Int,
    val quantity: Int,
    val image: String,
    val type: String
//    = "https://fastly.picsum.photos/id/381/536/354.jpg?hmac=UrahXEEnGTL3Aa0mUNERzMBn2X2Wf4POTUxC8MjRW1o"
)

